FROM ruby:2.7.2

ENV app /code
RUN mkdir $app
WORKDIR $app
ADD . $app

RUN chmod a+rwx -R /code

ENV HOME=/code

RUN bundle install

# Install Yarn.
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

# Run yarn install to install JavaScript dependencies.
RUN yarn install --check-files

# Set "rails server -b 0.0.0.0" as the command to
# run when this container starts.
# CMD ["rails", "server", "-b", "0.0.0.0", "-e", "development"]
CMD ["rails", "server", "-b", "0.0.0.0"]
